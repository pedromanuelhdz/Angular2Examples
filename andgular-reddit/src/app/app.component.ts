import { Component } from '@angular/core';
import { Article } from './article/article.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	articles: Article[];

	constructor(){
		this.articles = [
			new Article('AngularJS', 'angularjs.com', 3),
			new Article('Angular 2', 'angular2.com', 6),
			new Article('Angular 4', 'angular4.com', 4)
		];
	}

	addArticle(title: HTMLInputElement, link: HTMLInputElement){
		console.log(`Adding article title: ${title.value} and link ${link.value}`);
		return false;
	}
}
